# Data Science Tutorial

> This repository contains Python tutorials for concepts in Data Science to be used during the Onboarding Process.

## Table of Contents

1.  [About](#about)
2.  [Getting Started](#getting-started)
3.  [Onboarding Itinerary](#onboarding-itinerary)

---

## About

These notebooks were written and tested with Python 3.5, though other Python versions (including Python 2.7) should work in nearly all cases.

The book introduces the core libraries essential for working with data in Python: particularly [IPython](http://ipython.org), [NumPy](http://numpy.org), [Pandas](http://pandas.pydata.org), [Matplotlib](http://matplotlib.org), [Scikit-Learn](http://scikit-learn.org), and related packages.
Familiarity with Python as a language is assumed; if you need a quick introduction to the language itself, see the free companion project,
[A Whirlwind Tour of Python](https://github.com/jakevdp/WhirlwindTourOfPython): it's a fast-paced introduction to the Python language aimed at researchers and scientists.

---

## Getting Started

### Prerequites

- Ubuntu 16.04
- Python 3.5+

### Installation

1.  Clone this repository.

    ```bash
    $ git clone https://www.bitbucket.org/elucidatainc/data-science-tutorial
    ```

2.  Inside this directory, create a virtual environment and activate it.

    ```bash
    $ cd data-science-tutorial
    $ python3 -m venv .venv
    $ source activate .venv/bin/activate
    (.venv)$
    ```

3.  `pip` install requirements.

    ```bash
    (.venv)$ pip install -r requirements.txt
    ```

### Running Jupyter Notebooks

1.  Load virtual environment to Jupyter kernel.

    ```bash
    (.venv)$ python3 -m ipykernel install --user --name=.venv
    ```

2.  Run Jupyter Notebook.

    ```bash
    (.venv)$ jupyter notebook
    ```

3.  View Jupyter Notebook on the localhost: `localhost:8888`.

---

## Onboarding Itinerary

### Concepts in Big Data

1.  Statistics
    - Design of Experiments
    - Probability
    -
2.  Mathematics
    - Linear Algebra
    -
3.  Data Science by the Numbers for Biology
    - The signal in the noise
    -
4.  Curse of Dimensionality/Tyranny of Numbers

### Exploratory Data Analysis

- What language can we use to characterize data?
- Feature Engineering
- Preprocessing Data
  - Baseline Correction
  - Smoothing
  - Normalization
  - Alignment
  - Peak Picking
  - Binning

## Choosing a Learning Algorithm

- Objective Function
- Compute Complexity
- Optimizations and Cost Function
- Regularization and Overfitting
- Supervised Learning
- Unsupervised Learning

## Inference

- Statistical Inference
- Bayesian Inference
- Causal Inference

## Solving Big Data Problems

1.  As a Data Scientist

    - Dimensionality Reduction
    - Probabalistic Models
    - Feature Engineering
    - Noise-Channel Coding
    - Visualizations

2.  As an Engineer

    - Automation
    - Multithreading
    - Asynchronous Programming
    - MapReduce
    - Symbolic Programming
    - Cluster Computing
    - In-memory vs. GPU
    - Data Representation with Encoders and Decoders

3.  As a Product Manager

    - Communication
    - Problem Statement
